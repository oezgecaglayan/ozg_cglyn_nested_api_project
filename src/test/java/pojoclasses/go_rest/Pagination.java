package pojoclasses.go_rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagination {

    private int total;
    private int pages;
    private int page;
    private int limit;
}
