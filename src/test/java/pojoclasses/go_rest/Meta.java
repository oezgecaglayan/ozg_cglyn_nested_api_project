package pojoclasses.go_rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder


public class Meta {

    private Pagination pagination;
    private Links links;


}
