package pojoclasses.go_rest;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder

public class Add_Com {

    /**
     * {
     *     "code": 408,
     *     "meta": {
     *         "pagination": {
     *             "total": 2000,
     *             "pages": 211,
     *             "page": 1,
     *             "limit": 5,
     *             "links": {
     *                 "previous": null,
     *                 "current": "https://gorest.co.in/public/v1/comments?page=1",
     *                 "next": "https://gorest.co.in/public/v1/comments?page=2"
     *             }
     *         }
     *     },
     *     "data": [
     *         {
     *             "id": 2220,
     *             "post_id": 5585,
     *             "name": "Tecch Global",
     *             "email": "random email",
     *             "body": "random text"
     *         },
     *          {
     *             "id": 2221,
     *             "post_id": 5586,
     *             "name": "Tecch Global",
     *             "email": "random email",
     *             "body": "random text"
     *         },
     *          {
     *             "id": 2222,
     *             "post_id": 5587,
     *             "name": "Tecch Global",
     *             "email": "random email",
     *             "body": "random text"
     *         }
     *     ]
     * }
     *
     */


    private int code;
    private Meta meta;
    private Pagination pagination;
    private Links links;
    private List<Datas> data;

}
