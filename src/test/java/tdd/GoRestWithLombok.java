package tdd;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pojoclasses.go_rest.*;
import utils.ConfigReader;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;

public class GoRestWithLombok {


    Response response;
    Faker faker = new Faker();


    @BeforeTest
    public void setup() {
        RestAssured.baseURI = ConfigReader.getProperty("GoRestUrL");
    }


    @Test
    public void addAComment() {


        Pagination pagination = Pagination
                .builder()
                .total(2000)
                .pages(211)
                .page(1)
                .limit(5)
                .build();

        Links links = Links
                .builder()
                .previous("null")
                .current("https://gorest.co.in/public/v1/comments")
                .next("https://gorest.co.in/public/v1/comments")
                .build();


        Meta meta = Meta
                .builder()
                .pagination(pagination)
                .links(links)
                .build();

        Datas datas0 = Datas
                .builder()
                .id(2300)
                .post_id(5500)
                .name("Alaska")
                .email(faker.internet().emailAddress())
                .body("Hi everyone!")
                .build();

        Datas datas1 = Datas
                .builder()
                .id(2200)
                .post_id(5600)
                .name("Canada")
                .email(faker.internet().emailAddress())
                .body("Hi Canada!")
                .build();

        Datas datas2 = Datas
                .builder()
                .id(2100)
                .post_id(4500)
                .name("Turkey")
                .email(faker.internet().emailAddress())
                .body("Hi Turkey!")
                .build();

        Datas datas3 = Datas
                .builder()
                .id(2400)
                .post_id(2600)
                .name("Chicago")
                .email(faker.internet().emailAddress())
                .body("Hi Chicago!")
                .build();


        Add_Com add_com = Add_Com
                .builder()
                .code(300)
                .meta(meta)
                .pagination(pagination)
                .links(links)
                .data(Arrays.asList(datas0, datas1, datas2, datas3))
                .build();


        response = RestAssured
                .given().log().all()
                .header("Authorization" , ConfigReader.getProperty("GoRestToken"))
                .contentType(ContentType.JSON)
                .body(add_com)
                .when().post(ConfigReader.getProperty("GoRestUrL"))
                .then().log().all()
                .assertThat().statusCode(200)
                .time(Matchers.lessThan(8000L))
                .body("code", equalTo(422))
                .body("meta", equalTo(null))
                .body("data[0].message", equalTo("must exist"))
                .body("data[1].message", equalTo("is not a number"))
                .body("data[2].message", equalTo("can't be blank"))
                .body("data[3].message", equalTo("can't be blank, is invalid"))
                .extract().response();



    }
}

